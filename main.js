function addBudget() {

  let amount = document.getElementById('amount').value;
  let select = document.getElementById('type');
  let type = select.options[select.selectedIndex].value;
  let description = document.getElementById('description').value;

  amount = parseInt(amount);

  if (type != 0) {

    document.getElementById('error_divi').style.display = "none";
    document.getElementById('success_divi').style.display = "block";

    let data = {
      _id: Math.floor((Math.random() * 1000000) + 1),
      type: type,
      amount: amount,
      description: description
    }

    show_data_in_table(data);
    total(data);
    add_statistica(data);

  } else {

    document.getElementById('error_divi').style.display = "block";
    document.getElementById('success_divi').style.display = "none";

  }
}

function show_data_in_table(data) {

  if (data.type == "+") {
    $("#income > tbody").append(`
        <tr id="${data._id}" >
        <td>${data.amount}</td>
        <td>${data.description}</td>
        <td data-id="${data._id}" data-amount="${data.amount}" data-type="+" onclick="datani_sil(this)" class="sil btn btn-light"> Delete </td>
        </tr>`);
  } else {
    $("#expense > tbody").append(`
        <tr id="${data._id}">
        <td>${data.amount}</td>
        <td>${data.description}</td>
        <td data-id="${data._id}" data-amount="${data.amount}" data-type="-" onclick="datani_sil(this)" class="sil btn btn-light"> Delete </td>
        </tr>`);
  }

}

var income = 0;
var expense = 0;

function total(data) {
  if (data.type == "+") {
    income += data.amount;
    $('#total_income').text(income);
  } else {
    expense += data.amount;
    $('#total_expense').text(expense);
  }

  let my_budget = income - expense;

  $('#my_budget').text(my_budget);

}

function datani_sil(budget_tr) {

  let id = budget_tr.dataset.id;
  let type = budget_tr.dataset.type;
  let amount = budget_tr.dataset.amount;
    amount = parseInt(amount);
  if (confirm('Data silinsin?')) {
    $(`#${id}`).remove();
  } else {
    console.log('imtina');
  }
  let data= {
    type: type,
    amount:-amount,
  }
  total(data)
  add_statistica(data);
}

function add_statistica(data) {
  // Load google charts
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  var statistica_option  = [
    ['Task', 'Hours per Day'],
    ['Income', income],
    ['Expense', expense]
  ];

  // Draw the chart and set the chart values
  function drawChart() {
    var data = google.visualization.arrayToDataTable(statistica_option);

    // Optional; add a title and set the width and height of the chart
    var options = {'title':'My Average Day', 'width':550, 'height':400};

    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
  }
}
